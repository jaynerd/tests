﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Contacts
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : ContentPage
	{
        public event EventHandler<EventArgs> BackButtonPressed;
		public MainPage ()
		{
			InitializeComponent ();
		}

        public void Present(View view)
        {
            this.ContentPresenter.Content = view;
        }

        protected override bool OnBackButtonPressed()
        {
            BackButtonPressed?.Invoke(this, EventArgs.Empty);
            return true;
        }
    }
}