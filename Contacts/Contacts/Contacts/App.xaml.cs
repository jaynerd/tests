﻿using Contacts.IServices;
using Contacts.Services;
using Contacts.ViewModels;
using Contacts.Views;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Contacts
{
    public partial class App : Application
    {
        private ContactListViewModel _contactListViewModel;
        private IViewService _viewService;
        private IPermissionsService _permissionService;

        public App()
        {
            InitializeComponent();

            MainPage mainPage = new MainPage();
            mainPage.BackButtonPressed += MainPage_BackButtonPressed;
            MainPage = mainPage;
            _viewService = new ViewService(mainPage);
            _permissionService = new PermissionsService();

            _contactListViewModel = new ContactListViewModel(_viewService);
            _viewService.ShowViewForVm(_contactListViewModel);


        }


       
        private void MainPage_BackButtonPressed(object sender, EventArgs e)
        {
            GoBack();
        }

        /// <summary>
        /// Shoddy back handling but full view backstack not necessarily needed for a two-pager
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void GoBack()
        {
            if (_viewService.CurrentViewModel == _contactListViewModel)
            {
                MainPage.SendBackButtonPressed();
            }
            else
            {
                _viewService.ShowViewForVm(_contactListViewModel);
            }
        }

        protected async override void OnStart()
        {
            var permissionResult = await _permissionService.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Contacts);

            if (permissionResult == Plugin.Permissions.Abstractions.PermissionStatus.Unknown)
            {

            }
            else
            {
                ContactService contactService = new ContactService();

                var contacts = await contactService.ListAllContactsAsync();

                _contactListViewModel.PopulateFromList(contacts);
            }


        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
