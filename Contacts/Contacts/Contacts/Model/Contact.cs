﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contacts.Model
{
    public class Contact
    {
        public string Name { get; set; }
        public string Email{ get; set; }
        public string PrimaryPhoneNum { get; set; }
        public List<string> PhoneNums { get; set; }
        public string PhotoUri { get; internal set; }
    }
}
