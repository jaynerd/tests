﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Contacts.Model
{
    public class Settings
    {

        public string LastFavoriteContact { get; set; } = "";

        public void Save()
        {
            string fileName = GetSettingsPath();

            if (File.Exists(fileName))
                File.Delete(fileName);

            string fileContents = Newtonsoft.Json.JsonConvert.SerializeObject(this);
            File.WriteAllText(fileName, fileContents);
       }

        public static Settings Load()
        {
            string fileName = GetSettingsPath();

            if (File.Exists(fileName))
            {
                string fileContents = File.ReadAllText(fileName);
                Settings settings = Newtonsoft.Json.JsonConvert.DeserializeObject<Settings>(fileContents);
                return settings;
            }

            return new Settings();
        }

        public static string GetSettingsPath()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string filename = Path.Combine(path, "settings.json");

            return filename;
        }
    }
}
