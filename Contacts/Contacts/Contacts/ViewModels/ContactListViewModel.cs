﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Timers;
using Contacts.IServices;
using Contacts.Model;
using System.Linq;
using Xamarin.Forms;

namespace Contacts.ViewModels
{
    public class ContactListViewModel : ViewModelBase
    {
        private Settings _settings = null;
        private Timer _searchTimer;
        private object _synclock = new object();



        private readonly List<ContactItemViewModel> _fullContactList = new List<ContactItemViewModel>();

        private IViewService _viewService;

        public ContactListViewModel(IViewService viewService)
        {
            _viewService = viewService;

            _searchTimer = new Timer();
            _searchTimer.Interval = 100;
            _searchTimer.Elapsed += _searchTimer_Elapsed;
            _searchTimer.Stop();

            this.ViewFavoriteCommand = new Command(ViewFavorite);
        }


        private void ViewFavorite(object obj)
        {
            if (FavoriteContact != null)
                _viewService.ShowViewForVm(FavoriteContact);
        }

        public Command ViewFavoriteCommand { get; private set; }

        private ObservableCollection<ContactGroupViewModel> _contacts = new ObservableCollection<ContactGroupViewModel>();

        public ObservableCollection<ContactGroupViewModel> Contacts
        {
            get { return _contacts; }
            set
            {
                if (_contacts != value)
                {
                    _contacts = value;
                    base.RaisePropertyChanged("Contacts");
                }
            }
        }

        private ContactItemViewModel _favoriteContact = null;

        public ContactItemViewModel FavoriteContact
        {
            get { return _favoriteContact; }
            set
            {
                if (_selectedContact != value)
                {
                    _favoriteContact = value;
                    base.RaisePropertyChanged("FavoriteContact");
                    IsFavoriteVisible = (value != null);
                    if (value != null)
                    {
                        _settings.LastFavoriteContact = value.Name;
                        _settings.Save();
                    }
                }
            }
        }

        private ContactItemViewModel _selectedContact = null;

        public ContactItemViewModel SelectedContact
        {
            get { return _selectedContact; }
            set
            {
                if (_selectedContact != value)
                {
                    _selectedContact = value;
                    base.RaisePropertyChanged("SelectedContact");


                    if (value != null)
                        _viewService.ShowViewForVm(value);

                    SelectedContact = null;

                }
            }
        }

        private bool _isFavoriteVisible = false;

        public bool IsFavoriteVisible
        {
            get { return _isFavoriteVisible; }
            set
            {
                if (_isFavoriteVisible != value)
                {
                    _isFavoriteVisible = value;
                    base.RaisePropertyChanged("IsFavoriteVisible");
                }
            }
        }


        private string _searchText = "";

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (_searchText != value)
                {
                    _searchText = value;
                    StartSearch();
                    base.RaisePropertyChanged("SearchText");
                }
            }
        }


        internal void PopulateFromList(IEnumerable<Contact> contacts)
        {
            _settings = Settings.Load();

            foreach (var contact in contacts)
            {
                ContactItemViewModel ivm = new ContactItemViewModel(this, contact);
                _fullContactList.Add(ivm);

                if (_settings != null && ivm.Name == _settings.LastFavoriteContact)
                    FavoriteContact = ivm;
            }

            SearchText = "";
            ApplySearchFilter();
        }

        public void ApplySearchFilter()
        {
            ObservableCollection<ContactGroupViewModel> newCollection = new ObservableCollection<ContactGroupViewModel>();

            foreach (var group in _fullContactList.GroupBy(c => c.GroupKey))
            {
                ContactGroupViewModel vmGroup = new ContactGroupViewModel();
                vmGroup.GroupKey = group.Key;
                foreach (var contact in group)
                {
                    if (string.IsNullOrWhiteSpace(SearchText) || contact.Name.ToLowerInvariant().Contains(SearchText.ToLowerInvariant()))
                    {
                        vmGroup.Add(contact);
                    }
                }

                if (vmGroup.Any())
                    newCollection.Add(vmGroup);
            }

            Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                {
                    Contacts.Clear();
                    Contacts = newCollection;
                });

        }

        private bool _isSearching = false;
        private void StartSearch()
        {
            if (!_isSearching)
            {
                _searchTimer.Start();
                _isSearching = true;
            }
        }


        private void _searchTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _isSearching = false;
            _searchTimer.Stop();


            ApplySearchFilter();

        }


        private void SaveCurrentContactName()
        {

        }
    }
}
