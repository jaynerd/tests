﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace Contacts.ViewModels
{
    public class ContactGroupViewModel:ObservableCollection<ContactItemViewModel>
    {
        private string _groupKey = "";

        public string GroupKey
        {
            get { return _groupKey; }
            set
            {
                if (_groupKey != value)
                {
                    _groupKey = value;
                    base.OnPropertyChanged(new PropertyChangedEventArgs("GroupKey"));
                }
            }
        }

        //private List<ContactItemViewModel> _contacts = new List<ContactItemViewModel>();

        //public List<ContactItemViewModel> Contacts
        //{
        //    get { return _contacts; }
        //    set
        //    {
        //        if (_contacts != value)
        //        {
        //            _contacts = value;
        //            base.RaisePropertyChanged("Contacts");
        //        }
        //    }
        //}

        

    }
}
