﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Contacts.ViewModels
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        internal void OnShown()
        {
        }
    }
}
