﻿using System;
using System.Collections.Generic;
using System.Text;
using Contacts.IServices;
using Contacts.Model;
using Xamarin.Forms;

namespace Contacts.ViewModels
{
    public class ContactItemViewModel : ViewModelBase
    {
        public ContactItemViewModel(ContactListViewModel parent, Contact contact)
        {
            this.Parent = parent;

            PopulateFromContactModel(contact);
            SelectAsFavoriteCommand = new Command(OnSelectedAsFavorite);
            BackCommand = new Command(OnBack);
        }

        private void OnBack(object obj)
        {
            ((App)App.Current).GoBack();
        }

        private void OnSelectedAsFavorite(object obj)
        {
            if (Parent!=null)
            {
                Parent.FavoriteContact = this;
            }
        }


        public Command BackCommand { get; private set; }
        public Command SelectAsFavoriteCommand { get; private set; }
        public ContactListViewModel Parent { get; private set; }

        private string _groupKey = "";

        public string GroupKey
        {
            get { return _groupKey; }
            set
            {
                if (_groupKey != value)
                {
                    _groupKey = value;
                    base.RaisePropertyChanged("GroupKey");
                }
            }
        }


        private string _name = "";

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    base.RaisePropertyChanged("Name");
                }
            }
        }

        private string _email = "";

        public string Email
        {
            get { return _email; }
            set
            {
                if (_email != value)
                {
                    _email = value;
                    base.RaisePropertyChanged("Email");
                }
            }
        }

        private string _number = "";
        
        public string Number
        {
            get { return _number; }
            set
            {
                if (_number != value)
                {
                    _number = value;
                    base.RaisePropertyChanged("Number");
                }
            }
        }


        private string _imagePath = "";

        public string ImagePath
        {
            get { return _imagePath; }
            set
            {
                if (_imagePath != value)
                {
                    _imagePath = value;
                    base.RaisePropertyChanged("ImagePath");
                }
            }
        }

        public void PopulateFromContactModel(Contact contact)
        {
            this.Number = contact.PrimaryPhoneNum;
            this.Name = contact.Name;
            this.Email = contact.Email;
            this.ImagePath = contact.PhotoUri ?? "Contacts.Images.avatar.png";
            this.GroupKey = !string.IsNullOrWhiteSpace(this.Name ) ? this.Name[0].ToString() : "-";
        }


    }
}
