﻿using Contacts.IServices;
using Contacts.ViewModels;
using Contacts.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Contacts.Services
{
    public class ViewService : IViewService
    {
        private Dictionary<Type, View> _views = new Dictionary<Type, View>();
        private MainPage _mainPage;

        public ViewService(MainPage mainPage)
        {
            _mainPage = mainPage;
        }

        public event EventHandler<ViewModelBase> ViewRequested;

        public void ShowViewForVm(ViewModelBase vmBase)
        {
            ViewRequested?.Invoke(this, vmBase);

            Type requestedType = vmBase.GetType();
            View view = null;
            if (_views.ContainsKey(requestedType))
            {
                view = _views[requestedType];
            }
            else
            {
                if (vmBase.GetType() == typeof(ContactItemViewModel))
                {
                    view = new ContactDetailView();
                }
                else if (vmBase.GetType() == typeof(ContactListViewModel))
                {
                    view = new ContactListView();
                }
                else
                {
                    throw new NotImplementedException("Unknown view model type");
                }

                _views.Add(requestedType, view);
            }

            if (view == null)
                throw new Exception("Not able to find view for viewmodel");

            this.CurrentViewModel = vmBase;
            view.BindingContext = vmBase;
            vmBase.OnShown();
            _mainPage.Present(view);
        }

        public ViewModelBase CurrentViewModel { get; private set; }

        
    }
}
