﻿using Contacts.IServices;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contacts.Services
{
    public class PermissionsService : IPermissionsService
    {


        public async Task<PermissionStatus> CheckPermissionStatusAsync(Permission permission)
        {
            var results = await CrossPermissions.Current.RequestPermissionsAsync(permission);

            if (results.ContainsKey(permission))
            {
                return results[permission];
            }

            return PermissionStatus.Unknown;
        }
    }
}
