﻿using Contacts.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Contacts.Services
{
    public class ContactService
    {
        public ContactService()
        {

        }

        public async Task<IEnumerable<Contact>> ListAllContactsAsync()
        {
            List<Contact> contactList = new List<Contact>();

            IList<Plugin.ContactService.Shared.Contact> ccntacts = null;

            try
            {
                ccntacts= await Plugin.ContactService.CrossContactService.Current.GetContactListAsync();
            }
            catch(Exception ex)
            {
            }

            foreach(var ccontact in ccntacts)
            {
                Contact c = new Contact();
                c.Name= ccontact.Name;
                c.Email= ccontact.Email;
                c.PrimaryPhoneNum = ccontact.Number;
                c.PhoneNums = ccontact.Numbers;
                c.PhotoUri = ccontact.PhotoUri;


                contactList.Add(c);
            }

            //Contact c = new Contact();

            //c.FirstName = "James";
            //c.LastName = "Thomas";

            //contacts.Add(c);
            var sortedList = contactList.OrderBy(c => c.Name);

            return sortedList;
        }
    }
}
