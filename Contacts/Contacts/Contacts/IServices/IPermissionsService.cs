﻿using System.Threading.Tasks;
using Plugin.Permissions.Abstractions;

namespace Contacts.IServices
{
    public interface IPermissionsService
    {
        Task<PermissionStatus> CheckPermissionStatusAsync(Permission permission);
    }
}