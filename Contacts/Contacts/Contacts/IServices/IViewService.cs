﻿using System;
using Contacts.ViewModels;

namespace Contacts.IServices
{
    public interface IViewService
    {
        event EventHandler<ViewModelBase> ViewRequested;
        void ShowViewForVm(ViewModelBase vmBase);
        ViewModelBase CurrentViewModel { get; }
    }
}